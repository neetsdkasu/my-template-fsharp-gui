//////////////////////////////////////////////
// title  : Utils
// author : Leonardone @ NEETSDKASU
//////////////////////////////////////////////
module Utils

type Result<'a, 'b> =
    | Ok  of 'a
    | Err of 'b

module ResultUtils =
    module Result =
        let map apply res =
            match res with
            | Ok  res -> Ok (apply res)
            | Err err -> Err err

        let andThen run res =
            match res with
            | Ok  res -> run res
            | Err err -> Err err

        let iter apply res =
            match res with
            | Ok  res -> apply res
            | Err _   -> ()

        let get =
            function
            | Ok  res -> res
            | Err err -> failwith (sprintf "%A" err)

        let isOk =
            function
            | Ok  _ -> true
            | Err _ -> false

        let isErr =
            function
            | Ok  _ -> false
            | Err _ -> true

    type ResultBuilder() =
        member self.Bind(expr, func) = Result.andThen func expr
        member self.Return(expr)     = Ok expr
        member self.ReturnFrom(expr) = expr

    let result = new ResultBuilder()
