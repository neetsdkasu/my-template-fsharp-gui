@setlocal

@set program=program

@set sources= ^
    AssemblyInfo.fs ^
    Utils.fs ^
    Program.fs

@set flags= ^
    --codepage:65001 ^
    --target:winexe ^
    --out:%program%.exe ^
    --warnaserror+ ^
    --debug- ^
    --optimize+ ^
    --crossoptimize+ ^
    --standalone ^
    %* ^
    %sources%

fsc %flags%

@endlocal