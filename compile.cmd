@setlocal

@set program=program

@set sources= ^
    AssemblyInfo.fs ^
    Utils.fs ^
    Program.fs

@set flags= ^
    --codepage:65001 ^
    --target:winexe ^
    --out:%program%-debug.exe ^
    --warnaserror+ ^
    --debug+ ^
    %* ^
    %sources%

fsc %flags%

@endlocal