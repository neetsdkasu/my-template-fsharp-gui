//////////////////////////////////////////////
// title  :
// author : Leonardone @ NEETSDKASU
//////////////////////////////////////////////

open System.ComponentModel
open System.Drawing
open System.IO
open System.Windows.Forms

open Utils
open ResultUtils

let appTitle() : string = Application.ProductName

module UI =
    let setChildren (components: IContainer) (parent: Control) (children: Control array) : unit =
        for child in children do
            parent.Controls.Add(child)
            components.Add(child)


    let showMessage<'a, 'b> (res: Result<'a, 'b>) : unit =
        let (msg, title) =
            match res with
            | Ok  res -> (res.ToString(), appTitle())
            | Err err -> (sprintf "%A" err, "ERROR! - " + appTitle())
        MessageBox.Show(msg, title)
        |> ignore


    type MainForm() as self =
        inherit Form(StartPosition = FormStartPosition.CenterScreen
                    ,Width         = 500
                    ,Height        = 640
                    ,Text          = appTitle()
                    )
        let components = new Container()
        let mainMenu   = new MainMenu()
        let toolTip    = new ToolTip(components)
        do
            self.Menu <- mainMenu
        do
            setChildren components self [||]
        override self.Dispose (disposing: bool) : unit =
            if disposing then components.Dispose() else ()
            base.Dispose(disposing)


module Program =
    let run (args: string array) : Result<unit, exn> =
        try
            result {
                Application.EnableVisualStyles()
                Application.SetCompatibleTextRenderingDefault(false)
                let form  = new UI.MainForm()
                Application.Run(form)
                return! Ok ()
            }
        with
            err -> Err err


    [<System.STAThread>]
    [<EntryPoint>]
    let main (args: string array) : int =
        match run args with
        | Ok _ ->
            0
        | err ->
            UI.showMessage err
            1
