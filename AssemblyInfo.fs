//////////////////////////////////////////////
// author : Leonardone @ NEETSDKASU
//////////////////////////////////////////////
module AssemblyInfo

open System.Reflection
open System.Runtime.CompilerServices
open System.Runtime.InteropServices

// [<assembly: AssemblyTitle("MODULE NAME & DESCRIPTION (Not Application Title)")>]
[<assembly: AssemblyProduct("APPLICATION TITLE")>]
[<assembly: AssemblyDescription("APPLICATION DESCRIPTION")>]
[<assembly: AssemblyCompany("Leonardone @ NEETSDKASU")>]
[<assembly: AssemblyCopyright("(C) 2020 Leonardone @ NEETSDKASU")>]
[<assembly: ComVisible(false)>]
[<assembly: AssemblyVersion("1.0.0.0")>]
[<assembly: AssemblyFileVersion("1.0.0.0")>]
do ()
